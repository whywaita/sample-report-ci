#!/bin/sh

set -euo pipefail

REPORT_NAME=$1

git clone https://github.com/kakakaya/mics_report ${REPORT_NAME}
rm -rf ${REPORT_NAME}/.git
